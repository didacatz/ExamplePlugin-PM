<?php

namespace ExampleDir;

use pocketmine\event\Listener;
use pocketmine\event\player\PlayerJoinEvent;

class EventListener implements Listener # Now this is the listener, here you can write code execution when a event get's executed.
{
    protected $owner;

    public function __construct(ExamplePluginMainClass $owner)
    {
        $this->owner;
    }

    public function OnJoin(PlayerJoinEvent $event) # PlayerJoinEvent gets executed when a player join the server.
    {
        $player = $event->getPlayer(); # You can get the player with getPlayer()
        $player->sendMessage("Welcome to the server " . $player->getName() . "!"); # You can get the player name with getName().
    }
}