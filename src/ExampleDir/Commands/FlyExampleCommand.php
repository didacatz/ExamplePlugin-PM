<?php

namespace ExampleDir\Commands;

use ExampleDir\ExamplePluginMainClass;
use pocketmine\command\Command;
use pocketmine\command\CommandExecutor;
use pocketmine\command\CommandSender;
use pocketmine\Player;

class FlyExampleCommand implements CommandExecutor { # CommandExecutor means that this class will be able to execute a command

    /**@var ExamplePluginMainClass $plugin*/
    private $plugin;

    /**@param ExamplePluginMainClass $plugin*/
    public function __construct($plugin){
        $this->plugin = $plugin;
    }

    public function onCommand(CommandSender $sender, Command $cmd, $label, array $args) # What will happen on send a command.
    {
        $fcmd = $cmd->getName(); # With getName() you'll get what command the user is trying to execute.
        if($fcmd == "fly"){ # if the sender is executing /fly...
            if($sender instanceof Player){ # if the sender is a player and not a console or another one
                $sender->setGamemode(1); # set sender's gamemode creative
                $sender->sendMessage("Now you can fly!"); # send a message to the sender
            } else{
                $sender->sendMessage("Execute this command in-game!"); # send a message to the sender
            }
        }
    }
}
