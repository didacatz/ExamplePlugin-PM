<?php

namespace ExampleDir;

use ExampleDir\Commands\FlyExampleCommand;
use pocketmine\command\PluginCommand;
use pocketmine\plugin\PluginBase;
use pocketmine\utils\Config;

class ExamplePluginMainClass extends PluginBase # Now this is the main file.
{

    public function OnEnable() # Stuff that will happen when the plugin gets enabled.
    {
        # If you want use a Listener, you should say where you want register events.
        $this->getServer()->getPluginManager()->registerEvents(new EventListener($this), $this);
        # With getLogger() you'll get the console logger, where you can send some different types of messages.
        $this->getLogger()->info("ExamplePlugin was enabled");

        # This plugin is also using a command... well, you should register it!
        # you can register it inside plugin.yml or in your main class, I suggest you use this way.
        $mc_command = new PluginCommand('fly',$this);
        $mc_command->setExecutor(new FlyExampleCommand($this));
        # you should register also fly.command permission inside plugin.yml
        # I prefer register it inside plugin.yml
        $mc_command->setPermission('fly.command');
        $commandMap = $this->getServer()->getCommandMap();
        $commandMap->register('duels',$mc_command);

        # USING A CONFIG FILE.
        # you should create a resources folder and add the file with all the configurable options.
        $this->saveResource("example.yml");
        # Now we will create a class for it, this is the only way for get the content of the file.
        $config = new Config($this->getDataFolder() . "example.yml");
        # Now we'll get and info from it with get() and we'll log it.
        $this->getLogger()->info($config->get("enable-message"));
        # Now we'll set a variable inside it.
        $config->set("set-me", "This my new content!");
    }

    public function OnLoad() # Stuff that will happen when the plugin is loading.
    {
        $this->getLogger()->info("ExamplePlugin is loading");
    }

    public function OnDisable() # Stuff that will happen when the plugin gets disabled.
    {
        $this->getLogger()->info("ExamplePlugin was disabled");
    }
}