The most of people call 'ExampleDir' with the name of his plugin, but you should know that you can set it a custom name.
But is too important never repeat the same name, because this will brind to the plugin the namespace, and it should be
unique, because this is what difference your plugin from other plugin.
You can also do more than one dir, but remember that the namespace in every class should be the file position inside src
dir.

Examples:

If it's inside src, ExampleDir and ExampleDir2, it should be:
namespace ExampleDir\ExampleDir2;
If it's inside src, ExampleDir, ExampleDir2 and Task, it should be:
namespace ExampleDr\ExampleDir2\Task;

The file's name shouldn't be inside the namespace.